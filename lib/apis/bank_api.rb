require 'sinatra/base'
require 'validators/activation'
require 'nori'


module Api

  class BankApi < Sinatra::Base

    configure do
      set(:bind, '0.0.0.0')
      set(:server, 'webrick')
    end

    before do
      Api::Connection.connect
    end

    after do
      Api::Connection.disconnect
    end


    get '/find' do
      headers 'Content-Type' => 'text/xml'
      client_id   = Helper.parse_client_id(params[:clientid])
      phone_number  = Helper.parse_integer(params[:phonenum])
      valid = true
      luhm = true

      valid = false if client_id.nil? && !params[:clientid].blank?
      valid = false if phone_number.nil? && !params[:phonenum].blank?
      valid = false if phone_number.nil? && client_id.nil?

      if (valid && params[:clientid].length == 13) then
        if !Helper.check_luhm?(params[:clientid]) then
          valid = false
          luhm = false
        end
      end

      if (valid)

        viewer_fields_mapping = {
            "viewers_smartcard_id" => "viewer_smartcard_id",
            "viewers_phone" => "viewer_phone",
            "viewers_firstname" => "viewer_firstname",
            "viewers_lastname" => "viewer_lastname",
            "viewers_streetname" => "viewer_streetname",
            "viewers_housenum" => "viewer_housenum",
            "viewers_city" => "viewer_city",
            "viewers_postcode" => "viewer_postcode"
        }

        viewers_bouquets_fields_mapping = {
            "viewers_bouquets_bouquets_id" => "subscription_product_id",
            "viewers_bouquets_active_from" => "subscription_startdate",
            "viewers_bouquets_active_to" => "subscription_enddate"
        }


        viewer = Viewers.find_active(client_id,phone_number)
        if (viewer.respond_to? "each")
          viewer.each do |v|
            v.bouquets = Viewers_bouquets.get_by_viewer_id(v.id)
          end
        else
          viewer.bouquets = Viewers_bouquets.get_by_viewer_id(viewer.id)
          viewer = [viewer]
        end


        builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
          xml.response do
            Viewers_xml.produce_xml(xml,[viewer_fields_mapping,viewers_bouquets_fields_mapping],viewer)
          end
        end
        builder.to_xml
      else

        if (!luhm) then
          Helper.create_error_response("The checksum of ClientID is wrong")
        else
          Helper.create_error_response("You have provided wrong input parameters")
        end

      end
    end


    get'/listproducts' do
      headers 'Content-Type' => 'text/xml'

      product_fields_mapping = {
          "products_id" => "product_id",
          "products_name" => "product_name",
          "products_months" => "product_months",
          "products_bouquets_id" => "product_bouquets_id",
          "products_unit_length" => "product_unit_length",
          "products_unit" => "product_unit",
          "products_price" => "product_price",
          "products_hp_name" => "product_hp_name",
          "products_hp_id" => "product_hp_id"
      }

      products = Products.find_active
      if (!products.respond_to? "each")
        products = [products]
      end

      builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
        xml.response do
          Products_xml.produce_xml(xml,[product_fields_mapping],products)
        end
      end
      builder.to_xml
    end



    post '/process' do
      raw = request.env["rack.input"].read
      pp raw
      raw = raw.gsub!(/\n/, "").gsub!(/>\s*</, "><")
      parser = Nori.new
      response_hash = parser.parse(raw)
      $log.info "Request: #{raw}"
      if (!response_hash["CustomerInformationRequest"].nil?)
        customer(response_hash)
      elsif (!response_hash["PaymentNotificationRequest"].nil?)
        payment(response_hash)
      else
        Helper.create_error_response("Not supported request")
      end
    end

    def customer(response_hash)
      reason = ""
      viewers_smartcard_id = Helper.parse_client_id(response_hash["CustomerInformationRequest"]["CustReference"])
      product_number = Helper.parse_integer(response_hash["CustomerInformationRequest"]["PaymentItemCode"])
      merchant_reference = response_hash["CustomerInformationRequest"]["MerchantReference"]

      custReference = response_hash["CustomerInformationRequest"]["CustReference"]

      valid = true
      luhm = true

      valid = false if viewers_smartcard_id.nil? && !response_hash["CustomerInformationRequest"]["CustReference"].blank?
      valid = false if product_number.nil? && !response_hash["CustomerInformationRequest"]["PaymentItemCode"].blank?
      valid = false if product_number.nil? && viewers_smartcard_id.nil?

      if (valid && response_hash["CustomerInformationRequest"]["CustReference"].length == 13) then
        if !Helper.check_luhm?(response_hash["CustomerInformationRequest"]["CustReference"]) then
          reason = "Luhm check has faild for #{response_hash["CustomerInformationRequest"]["CustReference"]}"
          valid = false
          luhm = false
        else

        end
      end

      viewers = Viewers.find_all(viewers_smartcard_id) if (valid)
      products = Products.find_by_id(product_number) if (valid)
      customer_response = CustomerResponse.new(merchant_reference) if (valid)

      if (viewers.nil? || viewers.count == 0)
        valid = false 
        reason = "We have not found viewer in DB"
      end
      

      

      if (valid)


        @amount = 0

         #if (products.count > 0)
         # item = PaymentItem.new
         # item.productName = products.first.products_name
         # item.productCode = product_number
         # item.quantity = "1"
         # item.price = sprintf('%.2f', products.first.products_price)
         # item.subtotal = sprintf('%.2f',products.first.products_price)
         # item.tax = 0
         # item.total = sprintf('%.2f',products.first.products_price)
         # @amount = @amount + products.first.products_price
        #end

        if (viewers.count > 0)
          customer = Customer.new
          customer.status = viewers.first.viewers_is_active ? "0":"1"
          customer.customerReferenceAlternate = ""
          customer.customerReferenceDescription = ""
          customer.firstName = viewers.first.viewers_firstname
          customer.lastName = viewers.first.viewers_lastname
          customer.custReference = custReference
          customer.email = viewers.first.viewers_email.nil? ? "" : viewers.first.viewers_email
          customer.phone = viewers.first.viewers_phonenum.nil? ? "" : viewers.first.viewers_phonenum
          customer.amount = sprintf('%.2f',0)
        end

        #customer.items.push(item) if !item.nil? and !customer.nil?
        customer_response.customers.push(customer) if !customer.nil?

        builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
          customer_response.to_xml(xml)
        end

        $log.info "Success  #{builder.to_xml}"
        builder.to_xml

      else
        $log.info "The LUHM check didn't pass #{reason}"
        # If the LUHM check is not valid, we will return error message
        customer_response = CustomerResponse.new(merchant_reference)
        customer = Customer.new
        customer.status = "1";
        customer.custReference = custReference
        customer.amount = "0.00"
        customer_response.customers.push(customer)
        builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
          customer_response.to_xml(xml)
        end
        $log.info builder.to_xml
        builder.to_xml
      end

    end

    def payment(response_hash)

      if (!response_hash["PaymentNotificationRequest"]["Payments"]["Payment"].kind_of?(Array))
        payments_hash = [response_hash["PaymentNotificationRequest"]["Payments"]["Payment"]]
      else
        payments_hash = response_hash["PaymentNotificationRequest"]["Payments"]["Payment"]
      end

      begin
        payments_hash.each do |p_hash|
          if (!p_hash["PaymentItems"]["PaymentItem"].kind_of?(Array))
            payment_item_hash = [p_hash["PaymentItems"]["PaymentItem"]]
          else
            payment_item_hash = p_hash["PaymentItems"]["PaymentItem"]
          end

          @paymentLogId = p_hash["PaymentLogId"]

          Connection.connect

          existing_payment = Payments.where("id = #{p_hash["PaymentLogId"]}")
          fail "The payment with given ID is already in Database"  if (!existing_payment.nil? and !existing_payment.empty?)


          if (payment_item_hash.count == 1)

            payment = Payments.create({ :id => p_hash["PaymentLogId"],
                              :product_group_code => p_hash["ProductGroupCode"],
                              :cust_reference => p_hash["CustReference"],
                              :alternate_cust_reference => p_hash["AlternateCustReference"],
                              :amount => p_hash["Amount"],
                              :payment_status => p_hash["PaymentStatus"],
                              :payment_method => p_hash["PaymentMethod"],
                              :payment_reference => p_hash["PaymentReference"],
                              :terminal_id => p_hash["TerminalId"],
                              :channel_name => p_hash["ChannelName"],
                              :location => p_hash["Location"],
                              :is_reversal => p_hash["IsReversal"],
                              :payment_date => p_hash["PaymentDate"],
                              :settlement_date => p_hash["SettlementDate"],
                              :institution_id => p_hash["InstitutionId"],
                              :institution_name => p_hash["InstitutionName"],
                              :branch_name => p_hash["BranchName"],
                              :bank_name => p_hash["BankName"],
                              :fee_name => p_hash["FeeName"],
                              :customer_name => p_hash["CustomerName"],
                              :other_customer_info => p_hash["OtherCustomerInfo"],
                              :receipt_no => p_hash["ReceiptNo"],
                              :collections_account => p_hash["CollectionsAccount"],
                              :third_party_code => p_hash["ThirdPartyCode"],
                              :payment_item_name => payment_item_hash.first["ItemName"],
                              :payment_item_code => payment_item_hash.first["ItemCode"],
                              :payment_item_amount => payment_item_hash.first["ItemAmount"],
                              :payment_item_bank_code => payment_item_hash.first["LeadBankCode"],
                              :payment_item_cnb_code => payment_item_hash.first["LeadBankCbnCode"],
                              :payment_item_bank_name => payment_item_hash.first["LeadBankName"],
                              :bank_code => p_hash["BankCode"],
                              :customer_address => p_hash["CustomerAddress"],
                              :customer_phone_number => p_hash["CustomerPhoneNumber"],
                              :depositor_name => p_hash["DepositorName"],
                              :deposit_slip_number => p_hash["DepositSlipNumber"],
                              :payment_currency => p_hash["PaymentCurrency"],
                              :processed => 0,
                              :created_at => DateTime.now(),
                              :updated_at => DateTime.now()
                            })

            customReference = ""
            if (p_hash["CustReference"].length == 13) then
              customReference = p_hash["CustReference"][0..10]
            else
              customReference = p_hash["CustReference"]
            end

            response = Products.proc_orderproduct2(customReference,payment_item_hash.first["ItemCode"])
            $log.info "Response is #{response}"
            if (response == "1")
              $log.info "Payment was procesed succefully"
              payment.processed = 1
              payment.updated_at = DateTime.now()
              payment.save
            else
              fail "We have not process the request"
            end
            Connection.disconnect
          else
            fail "There is more then one payment item in requested query"
          end
        end
        createPaymentResponse(payments_hash,0)
      rescue => e
        Connection.disconnect
        $log.info "Payment was not procesed succefully #{e.message}"
        createPaymentResponse(payments_hash,1)
      end

    end

    def createPaymentResponse(payments_hash,status)
      payments = []
      payments_hash.each do |inner|
        payments.push(PaymentResponse.new(inner["PaymentLogId"],status))
      end

      builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
        xml.PaymentNotificationResponse do
          xml.Payments do
            payments.each do |payment|
              payment.to_xml(xml)
            end
          end
        end
      end
      builder.to_xml
    end




  end
end