require 'sinatra/base'
require 'validators/activation'

module Api

  class SmsApi < Sinatra::Base

    configure do
      set(:bind, '0.0.0.0')
      set(:server, 'webrick')
    end

    post '/sms/incoming.php' do
      #headers 'Content-Type' => 'text/xml'

      s_function = params[:s_function]
      s_session = params[:s_session]   #message_session
      s_type = params[:s_type]
      s_msisdn = params[:s_msisdn] #essage_origin
      s_param = params[:s_param]
      p_msg = params[:p_msg] #message_text
      p_sn = params[:p_sn] #message_destination

      Connection.connect


      MoMessage.create({:message_session => s_session,:message_origin => s_msisdn,:message_text => p_msg,:message_destination => p_sn,:message_status => 0,:message_loaded => DateTime.now()})
      Connection.disconnect

      builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
        xml.response do
          xml.status "OK"
          xml.message "We have received message with following parameters: s_function = #{s_function} s_session = #{s_session} s_type = #{s_type} s_msisdn = #{s_msisdn} s_param = #{s_param} p_msg = #{p_msg} p_sn = #{p_sn} "
        end
      end
      builder.to_xml






      #type = "SMS:TEXT"
      #account = "Test Account"
      #shortcode = "1234"
      #phonenumber = "123456789"
      #message = "Dummy message"
      #
      #template_file = File.open("lib/templates/sms_reply.txt", "r").read
      #erb = ERB.new(template_file)
      #erb.result(binding)
      end
  end

end
