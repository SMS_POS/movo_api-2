require "active_support/all"
require "active_record"
require "validatable"
require "data/connection"
require "nokogiri"

%w(viewers viewers_bouquets products authoriselog activatelog mo_message payment_response customer_response payments authoriselog_fmw viewers_invoices).each {|a| require "data/#{a}"}
%w(helper).each {|a| require "helpers/#{a}"}
%w(activation).each {|a| require "validators/#{a}"}
%w(authentication).each {|a| require "authentication/#{a}"}
%w(bank_api hp_api sms_api android_api first_mobile_wallet_api json_bank_api).each {|a| require "apis/#{a}"}

module Api

  class << self

    def start_bank_api(port)
      ActiveRecord::Base.logger = $log
      ActiveRecord::Base.logger.level = Logger::DEBUG
      BankApi.run!({:port => port})
    end

    def start_json_bank_api(port)
      ActiveRecord::Base.logger = $log
      ActiveRecord::Base.logger.level = Logger::DEBUG
      JsonBankApi.run!({:port => port})
    end


    def start_sms_api(port)
      ActiveRecord::Base.logger = $log
      SmsApi.run!({:port => port})
    end

    def start_hp_api(port)
      ActiveRecord::Base.logger = $log
      ActiveRecord::Base.logger.level = Logger::DEBUG
      HpApi.run!({:port => port})
    end

    def start_android_api(port)
      ActiveRecord::Base.logger = $log
      ssl_options = {
          :cert_chain_file => 'certificate/ssl-cert-snakeoil.pem',
          :private_key_file => 'certificate/ssl-cert-snakeoil.key',
          :verify_peer => false
      }

      AndroidApi.run!() do |server|
        server.ssl = true
        server.ssl_options = ssl_options
      end

    end

    def start_first_mobile_wallet_api(port)
      ActiveRecord::Base.logger = $log
      ssl_options = {
          :cert_chain_file => 'certificate/ssl-cert-snakeoil.pem',
          :private_key_file => 'certificate/ssl-cert-snakeoil.key',
          :verify_peer => false
      }

      FirstMobileWalletApi.run!() do |server|
        server.ssl = true
        server.ssl_options = ssl_options
      end

    end





    def test
      puts "test"
    end


  end


end