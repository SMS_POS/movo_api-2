require 'sinatra/base'
require 'validators/activation'

module Api

  # hp_api - Haiti - NullTV



  class HpApi < Sinatra::Base

    configure do
      set(:bind, '0.0.0.0')
    end

    before do
      Api::Connection.connect
    end

    after do
      Api::Connection.disconnect
    end


    get '/test' do
      "ahoj"
    end

    post '/order' do
      headers 'Content-Type' => 'text/xml'
      client_id   = params[:clientid]
      product_id  = params[:product]
      order = params[:order]

      valid = true
      valid = false if params[:clientid].blank?
      valid = false if params[:product].blank?

      if (!valid) then
        return Helper.create_error_response("Some of the input parameters are empty. Requested parameters: clientid,product")
      end

      if (params[:clientid].length == 13 && !Helper.check_luhm?(params[:clientid])) then
        return Helper.create_error_response("ClientId is not valid. Checksum check - FAILED")
      end

      parsed_id=Helper.parse_client_id(client_id)
      parsed_product=Helper.parse_product_id(product_id)

      valid = false if client_id.nil?
      valid = false if product_id.nil?

      $log.debug request.fullpath
      $log.debug "Parsed_id is #{parsed_id}"
      $log.debug "Parsed_product is #{parsed_product}"

      if (!valid) then
        return Helper.create_error_response("You have provided wrong input parameters. ")
      end

      if (!Viewers.exists?(parsed_id))
        return Helper.create_error_response("The clientId don't exist in database.")
      end

      if (!Products.exists?(parsed_product))
        return Helper.create_error_response("The productId don't exist in database.")
      end

      AuthoriseLog.create({:authoriselog_date => DateTime.now,:authoriselog_request => Helper.create_log_message(params),:authoriselog_parsed => "#{parsed_id}#{parsed_product}"})
      result = Products.proc_orderproduct2(parsed_id,parsed_product)

      builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
        xml.response do
                if (result=="1")
                  xml.status "OK"
                  xml.message "Input parsed correctly"
                else
                  xml.status "ERROR"
                  xml.message "There was error in "
                end
        end
      end
      builder.to_xml
    end

    post '/activate' do
      headers 'Content-Type' => 'text/xml'
      viewer = Activation.new
      viewer.chipid = params[:chipid]
      viewer.firstname = params[:firstname]
      viewer.lastname =  params[:lastname]
      viewer.nif = params[:nif]
      viewer.address = params[:address]
      viewer.zone = params[:zone]
      viewer.phonenum = params[:phonenum]
      viewer.email = params[:email]



      if (!viewer.valid?)
        pp viewer.errors
        builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
          xml.response do
              xml.status "ERROR"
              xml.message "Mandatory parameters not passed or not valid. Error: #{Helper.parse_validation_error(viewer.errors)}"
          end
        end
        builder.to_xml
      else
        smartcard_id = Viewers.getSmartCardId(viewer.chipid)
        if (smartcard_id.nil?) then
          return Helper.create_error_response("You have provided wrong ChipId. There is no un-activated viewer connected to this ChipId.")
        end
        smartcard_id = smartcard_id["viewers_smartcard_id"]

        ActivateLog.create({:activatelog_date => DateTime.now,:activatelog_request => Helper.create_log_message(params)})

        builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
          xml.response do
              result = Viewers.callActivate(viewer)
              xml.status "OK"
              xml.message "#{smartcard_id}"
          end
        end
        builder.to_xml
      end
    end
















  end


end
