module Api

  class Activation
    include Validatable

    validates_presence_of :firstname,:lastname,:nif,:address,:zone,:chipid,:phonenum
    validates_length_of :firstname,:lastname,:nif, :maximum=>20
    validates_length_of :address,:email, :maximum=>100
    validates_length_of :zone, :maximum=>30
    validates_format_of :email, :with => /\b[A-Z0-9._%a-z\-]+@(?:[A-Z0-9a-z\-]+\.)+[A-Za-z]{2,4}\z/, :if => Proc.new { !email.nil? }
    validates_numericality_of :chipid, :phonenum

    attr_accessor :firstname,:lastname,:nif,:address,:zone,:chipid,:phonenum,:email

  end



end
