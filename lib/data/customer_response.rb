module Api

  class CustomerResponse
    attr_accessor :merchantReference,:customers

    def initialize(merchantReference)
      @merchantReference = merchantReference
      @customers = []
    end

    def to_xml(xml)
        xml.CustomerInformationResponse do
          xml.MerchantReference @merchantReference
          xml.Customers do
            @customers.each do |customer|
              customer.to_xml(xml)
            end
          end
        end
    end


  end

  class Customer

    attr_accessor :status,:custReference,:customerReferenceAlternate,:customerReferenceDescription,:firstName,:lastName,:otherName,:email,:phone,:thirdPartyCode,:amount,:items

    def initialize
      @items = []
    end

    def to_xml(xml)
      xml.Customer do
        xml.Status @status
        xml.CustReference @custReference
        xml.CustomerReferenceAlternate @customerReferenceAlternate
        xml.CustomerReferenceDescription @customerReferenceDescription
        xml.FirstName @firstName
        xml.LastName @lastName
        xml.OtherName @otherName
        xml.Email @email
        xml.Phone @phone
        xml.ThirdPartyCode @thirdPartyCode
        xml.Amount @amount
      end
    end


  end

  class PaymentItem
    attr_accessor :productName,:productCode,:quantity, :price,:subtotal, :tax, :total

    def to_xml(xml)
      xml.Item do
        xml.ProductName @productName
        xml.ProductCode @productCode
        xml.Quantity @quantity
        xml.Price @price
        xml.Subtotal @subtotal
        xml.Tax @tax
        xml.Total @total
      end

    end


  end




end


