require 'sinatra/base'
require 'validators/activation'
require "openssl"

module Api

  # FirstMobileWalletApi - Dimitri



  class FirstMobileWalletApi < Sinatra::Base

    use Rack::Auth::Basic, "Protected Area" do |username, password|
      @username = username
      $log.info password
      username == 'admin' && password == 'admin'
    end


    configure do
      set(:server, 'thin')
      set(:port,6082)
      set(:bind,'0.0.0.0')
      set(:logging,true)
    end


    before do
      Api::Connection.connect
    end

    after do
      Api::Connection.disconnect
    end

    get '/test' do
      "ahoj"
    end

    post '/order' do
      headers 'Content-Type' => 'text/xml'
      client_id   = params[:clientid]
      product_id  = params[:product]
      transid = params[:transid]
      order = params[:order]

      $log.info "Received parameters: clientid: #{params[:clientid]} product_id: #{params[:product]} transid: #{params[:transid]}"

      valid = true
      valid = false if params[:clientid].blank?
      valid = false if params[:product].blank?
      valid = false if params[:transid].blank?

      if (!valid) then
        return Helper.create_error_response("Some of the input parameters are empty. Requested parameters: clientid,product,transid")
      end

      if (params[:clientid].length == 13 && !Helper.check_luhm?(params[:clientid])) then
        return Helper.create_error_response("ClientId is not valid. Checksum check - FAILED")
      end

      parsed_id=Helper.parse_client_id(client_id)
      parsed_product=Helper.parse_product_id(product_id)

      valid = false if client_id.nil?
      valid = false if product_id.nil?

      $log.debug request.fullpath
      $log.debug "Parsed_id is #{parsed_id}"
      $log.debug "Parsed_product is #{parsed_product}"

      if (!valid) then
        return Helper.create_error_response("You have provided wrong input parameters. ")
      end

      if (!Viewers.exists?(parsed_id))
        return Helper.create_error_response("The clientId don't exist in database.")
      end


      if (!Products.exists?(parsed_product))
        return Helper.create_error_response("The productId don't exist in database.")
      end
      AuthoriseLog.create({"authoriselog_date" => DateTime.now,"authoriselog_request" => Helper.create_log_message(params).to_s,"authoriselog_parsed" => "#{parsed_id}#{parsed_product}"})
      result = Products.proc_order_mobile_wallet(parsed_id,parsed_product,transid)
      Api::Connection.disconnect
      builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
        xml.response do
                if (result=="1")
                  xml.status "OK"
                  xml.message "Input parsed correctly"
                else
                  xml.status "ERROR"
                  xml.message "There was error in database processing (return value #{result})"
                end
        end
      end
      builder.to_xml
    end

    post '/activate' do
      headers 'Content-Type' => 'text/xml'
      viewer = Activation.new
      viewer.chipid = params[:chipid]
      viewer.firstname = params[:firstname]
      viewer.lastname =  params[:lastname]
      viewer.nif = params[:nif]
      viewer.address = params[:address]
      viewer.zone = params[:zone]
      viewer.phonenum = params[:phonenum]
      viewer.email = params[:email]



      if (!viewer.valid?)
        pp viewer.errors
        builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
          xml.response do
              xml.status "ERROR"
              xml.message "Mandatory parameters not passed or not valid. Error: #{Helper.parse_validation_error(viewer.errors)}"
          end
        end
        builder.to_xml
      else
        smartcard_id = Viewers.getSmartCardId(viewer.chipid)
        if (smartcard_id.nil?) then
          return Helper.create_error_response("You have provided wrong ChipId. There is no un-activated viewer connected to this ChipId.")
        end
        smartcard_id = smartcard_id["viewers_smartcard_id"]

        ActivateLog.create({:activatelog_date => DateTime.now,:activatelog_request => Helper.create_log_message(params)})
        Api::Connection.disconnect

        builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
          xml.response do
              result = Viewers.callActivate(viewer)
              xml.status "OK"
              xml.message "#{smartcard_id}"
          end
        end
        builder.to_xml
      end
    end


    get'/listproducts' do
      headers 'Content-Type' => 'text/xml'

      product_fields_mapping = {
          "products_id" => "product_id",
          "products_name" => "product_name",
          "products_months" => "product_months",
          "products_bouquets_id" => "product_bouquets_id",
          "products_unit_length" => "product_unit_length",
          "products_unit" => "product_unit",
          "products_price" => "product_price",
          "products_hp_name" => "product_hp_name",
          "products_hp_id" => "product_hp_id"
      }

      products = Products.find_active
      if (!products.respond_to? "each")
        products = [products]
      end

      builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
        xml.response do
          Products_xml.produce_xml(xml,[product_fields_mapping],products)
        end
      end
      builder.to_xml
    end


    get '/find' do
      headers 'Content-Type' => 'text/xml'
      client_id   = Helper.parse_client_id(params[:clientid])
      phone_number  = Helper.parse_integer(params[:phonenum])
      valid = true
      luhm = true

      valid = false if client_id.nil? && !params[:clientid].blank?
      valid = false if phone_number.nil? && !params[:phonenum].blank?
      valid = false if phone_number.nil? && client_id.nil?

      if (valid && params[:clientid].length == 13) then
        if !Helper.check_luhm?(params[:clientid]) then
          valid = false
          luhm = false
        end
      end

      if (valid)

        viewer_fields_mapping = {
            "viewers_smartcard_id" => "viewer_smartcard_id",
            "viewers_phone" => "viewer_phone",
            "viewers_firstname" => "viewer_firstname",
            "viewers_lastname" => "viewer_lastname",
            "viewers_streetname" => "viewer_streetname",
            "viewers_housenum" => "viewer_housenum",
            "viewers_city" => "viewer_city",
            "viewers_postcode" => "viewer_postcode"
        }

        viewers_bouquets_fields_mapping = {
            "viewers_bouquets_bouquets_id" => "subscription_product_id",
            "viewers_bouquets_active_from" => "subscription_startdate",
            "viewers_bouquets_active_to" => "subscription_enddate"
        }


        viewer = Viewers.find_active(client_id,phone_number)
        if (viewer.respond_to? "each")
          viewer.each do |v|
            v.bouquets = Viewers_bouquets.get_by_viewer_id(v.id)
          end
        else
          viewer.bouquets = Viewers_bouquets.get_by_viewer_id(viewer.id)
          viewer = [viewer]
        end


        builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
          xml.response do
            Viewers_xml.produce_xml(xml,[viewer_fields_mapping,viewers_bouquets_fields_mapping],viewer)
          end
        end
        builder.to_xml
      else

        if (!luhm) then
          Helper.create_error_response("The checksum of ClientID is wrong")
        else
          Helper.create_error_response("You have provided wrong input parameters")
        end

      end
    end


    get '/status' do
      headers 'Content-Type' => 'text/xml'
      transid = params[:transid]

      valid = true
      validation_message = ""

      if transid.nil? or transid == ""
        valid = false
        validation_message = "Id is not valid (most likely empty)!"
      end

      if valid
        status = AuthoriselogFmw.find_by_authoriselog_tranid(transid)
      end
      if status.nil? and valid
        valid = false
        validation_message = "Id is not in database"
      end


      if (valid)
        builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
          xml.response do
            xml.transactionid transid
            xml.status status.authoriselog_status
            xml.productid status.authoriselog_products_id
            xml.type status.authoriselog_type
            xml.date status.authoriselog_date
            xml.viewersid status.authoriselog_viewers_id
            xml.bouquetsid status.authoriselog_bouquets_id
          end
        end
        builder.to_xml
      else
        builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
          xml.response do
            xml.status "ERROR"
            xml.message validation_message
          end
        end
        builder.to_xml
      end
    end















  end


end
