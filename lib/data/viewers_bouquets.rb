module Api

  class Viewers_bouquets < ActiveRecord::Base

    def self.get_by_viewer_id(id)
      conditions = {}
      conditions[:viewers_bouquets_viewers_id] = id
      conditions[:viewers_bouquets_cancelled] = 0
      where(conditions)
    end


  end

  class Viewers_bouquets_xml


    def self.produce_xml(xml,fields,datas)
      xml.subscriptions do
        datas.each do |data|
          xml.subscription do |inner|
            fields.each do |key,value|
              inner.send(value, data[key])
            end
          end
        end
      end
    end
  end
end