module Api

  class Helper

    def self.parse_product_id(product_id)
      $log.debug "parsing #{product_id}"
      return safeParse2(product_id)
    end

    def self.parse_integer(value)
      $log.debug "parsing #{value}"
      return safeParse2(value)
    end


    def self.parse_client_id(client_id)
      $log.debug "parsing #{client_id}"
      if client_id.blank? then return nil end
      if client_id.length == 13
        control=client_id.split('-')[1].to_i
        smartcard_id=safeParse2(client_id.split('-')[0])
      end
      if (client_id.length == 11 || client_id.length == 10)
        smartcard_id=safeParse2(client_id)
      end
      return smartcard_id
    end



    def self.check_luhm?(num)
      odd = true
      num.to_s.gsub(/\D/,'').reverse.split('').map(&:to_i).collect { |d|
        d *= 2 if odd = !odd
        d > 9 ? d - 9 : d
      }.sum % 10 == 0
    end


    def self.fill_template(template,data)
      template_file = File.open("lib/templates/#{template}", "r").read
      @data = data
      erb = ERB.new(template_file)
      erb.result(binding)
    end

    def self.create_error_response(error)
      builder = Nokogiri::XML::Builder.new(:encoding => 'UTF-8') do |xml|
        xml.response do
          xml.status "ERROR"
          xml.error error
        end
      end
      builder.to_xml
    end

    def self.safeParse2(strToParse)
      if strToParse =~ /^-?[0-9]+$/
        strToParse.to_i
      else
        nil
      end
    end

    def self.create_log_message(params)
      test=""
      params.each do |field|
        test << field.to_s
      end
    end

    def self.parse_validation_error(error)
      text = ""
      error.errors.each do |key,value|
        text = text + 'The parameter #{key} #{value.first} \n'
      end
      text
    end


  end


end