CREATE TABLE `viewers_invoices` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`viewers_id` INT(10) NOT NULL DEFAULT '0',
	`product_id` INT(10) NOT NULL DEFAULT '0',
	`user_id` INT(10) NOT NULL DEFAULT '0',
	`text` TEXT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_bin'
ENGINE=InnoDB;
