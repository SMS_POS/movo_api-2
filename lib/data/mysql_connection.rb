
module Api

  class MySqlConnection

    attr_accessor :client

    @@instance = MySqlConnection.new

    def self.instance
      return @@instance
    end

    def connect(host,username,password,database)
      @host = host
      @username = username
      @password = password
      @database = database

      begin
        @client = Mysql2::Client.new(:host => @host, :username => @username, :password => @password, :database => @database)
        puts "Connected"
      rescue
        fail "Unable to connect to DB"
      end
    end

    def logToAuthoriseLog(authoriselog_request,parsed_id,parsed_product)
      @client.query "INSERT INTO authoriselog (`authoriselog_date`,`authoriselog_request`,`authoriselog_parsed`) VALUES (now(),'#{authoriselog_request}','#{parsed_id}#{parsed_product}');"
    end


    def getSmartCardId(chipid)
      @client.query("SELECT viewers_smartcard_id FROM viewers WHERE viewers_settopbox_id = '#{chipid}' and viewers_is_active=0 LIMIT 1")
    end

    def logToActivateLog(activatelog_request)
      @client.query "INSERT INTO activatelog.rb (`activatelog_date`,`activatelog_request`) VALUES (now(),'#{activatelog_request}');"
    end

    def callOrderProduct2(parsed_id,parsed_product)
      @client.query "call proc_orderproduct2('#{parsed_id}','#{parsed_product}',@returnValue);"
      @client.query("select @returnValue as returnValue;");
    end

    def callActivate(viewer)
      @client.query("call proc_activate('#{viewer.chipid}','#{viewer.firstname}','#{viewer.lastname}','#{viewer.phonenum}','#{viewer.nif}','#{viewer.address}','#{viewer.zone}');")
    end

    def getViewers(cliendid,phonenumber)
      @client.query("SELECT * FROM viewers WHERE viewers_id = 155;")
    end


  end


end